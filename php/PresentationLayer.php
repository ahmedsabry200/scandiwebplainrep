<?php
namespace PresentationLayer;

include_once ('./php/BusinessLogicLayer.php');
use BusinessLogicLayer;
use BusinessLogicLayer\BLL;
use BusinessLogicLayer\book;
use BusinessLogicLayer\furniture;
use BusinessLogicLayer\DVD;

class  PL
{
    // here we will create boxes for each product to display in page
    private static function Create_HTML_Products_Boxes( $Products )
    {    
        $str="";
        foreach ($Products as $product) {
            $str .= "<div class='product_container'>
            <input type='checkbox' class='delete-checkbox' name ='Delete-checkbox[]' value='".$product->GetSku().",".$product->GetType()."'/>";
            foreach ($product->GetData() as  $k => $v ) {
                $str .="<p>".$v."</p> ";
            }
            $str .="</div>";
        }
        return $str;
    }
    // create product based on it's type and delete it
    public static function  DeleteProducts($Data)
    {
        foreach ($Data as $value) {
        self::GetSkuAndTypeFromText($value, $Sku, $Type);
        $product = new  $Type($Sku);
        $product->DeleteFromDB();
        }
    }
    // get the type and sku for deleted product 
    private static function  GetSkuAndTypeFromText($value,&$Sku,&$Type)
    {
        $position = strripos($value, ",");
        $Sku = substr($value, 0, $position);
        $Type = "BusinessLogicLayer"."\\".substr($value,$position+1);
    }
    // here we can view all books
    public static  function View_All_Books( )
    {
        $Books=BLL::GetAllBooks();
        $str= self::Create_HTML_Products_Boxes( $Books );
        return $str;
    }
    // Here we can view all products
    public static function View_All_Products( )
    {
        $Products=BLL::GetAllProducts();
        $str= self::Create_HTML_Products_Boxes( $Products );
        return $str;
    }

    public static function SaveProduct($productData,&$ErrorMesssage=null)
    {
        $type="BusinessLogicLayer"."\\". $productData['select_Type'];
        $product= new $type($productData['Sku']);
        if ($product->Fill($productData)) {
            if($product->AddToDb($ErrorMesssage))
                return true;
        }
        return false;
    }
}


