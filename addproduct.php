<!doctype html>
    <html>
      <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>TEST ASSESMENT</title>
      <link rel="stylesheet" href="Styles/PL.css" type="text/css">
      <link rel="icon" href="Images/icon.png" type="image/icon type">
      </head>
      <body>
          <!----------------------php code------------------------->
          <?php
              use PresentationLayer\PL;
              include_once ('./php/PresentationLayer.php');
              $ErrorMessage='';
              if( isset($_POST['Save'])){
                  if(PL::SaveProduct($_POST,$ErrorMessage) ){
                      header("Location: http://ahmedsabry.info");
                  }
              }
              if(isset($_POST['Cancel'])){
              header("Location: http://ahmedsabry.info");
              }
          ?>
          <!----------------------------------------------->
          <div class="Products_container">
              <form id="product_form" action="<?=$_SERVER['PHP_SELF'];?>" onsubmit="return validateFields();" method="Post"> 
                  <h1>Add a Product </h1>
                  <div class="Buttons">
                      <button type="submit" id="SaveButton" name="Save" text=""  onclick="AllowValidation()">Save</button>
                      <button type="submit" id="CancelButton" name="Cancel" onclick="PreventValidation()">Cancel</button>
                  </div>
                  <hr/>
                  <table ID="FormTable">
                      <tr>
                          <td>
                              <label id ="skulabel"  for="sku">SKU</label>
                          </td>
                          <td class="inputcell">
                              <input type="text" id="sku" name="Sku" require></input><span class="Validation"> </span>
                          </td>
                      </tr>
                      <tr>
                          <td>
                              <label id ="Namelabel" for="name">Name </label>
                          </td>
                          <td class="inputcell">
                              <input type="text" id="name" name="Name" require></input><span class="Validation"></span>
                          </td>
                      </tr>
                      <tr>
                          <td>
                              <label id ="Pricelabel" for="price">Price($)</label>
                          </td>
                          <td class="inputcell">
                              <input type="number" id="price"  min="0" step="0.01" name="Price" require><span class="Validation"></span>
                              </input>
                          </td>
                      </tr>
                      <tr>
                          <td>
                              <label id ="typeSwitcherLabel" for="productType">Type Switcher</label>
                          </td>
                          <td>
                              <select id ="productType"  name="select_Type" onchange='CreateOptionForm(this)' require>
                                  <option value="">--------ChooseType---------</option>
                                  <option value="Book" >Book</option>
                                  <option value="DVD">DVD</option>
                                  <option value="Furniture">Furniture</option>
                              </select>
                              <span class="Validation"> </span>
                          </td>
                          <td>
                              <label id ="productTypeLabel" for="productType" class="Validation"></label>
                          </td>
                      </tr>
                  </table>
                  <div id="DynamicContent">
                  </div>
                  <label id ="FormValidation"  >
                  <!------------php code---------------------->
                  <?php
                      if ($ErrorMessage != '') {
                          echo $ErrorMessage;
                      }
                  ?>
                  <!---------------------------------->
                  </label>
              </form>

          </div>
          <footer>
              <hr/>
              <p id="footer_text"> Scandiweb Test assignment </p>
          </footer>
          <script src="Scripts/JS.js">  </script>
          <script>
          formload();
          window.onunload = function(){
          var select = document.getElementById('productType');
          select.selectedIndex=0;
          }; 
          </script>
      </body>
    </html>