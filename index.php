<!DOCTYPE html>
<html lang="en">
<head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>TEST ASSESMENT</title>
      <link rel="stylesheet" href="Styles/PL.css" type="text/css">
      <link rel="icon" href="Images/icon.png" type="image/icon type">
</head>
<body>
   <?php
       use PresentationLayer\PL;
       include_once ('./php/PresentationLayer.php');
                 
       if(isset($_POST['Delete-checkbox'])&&isset($_POST['Delete'])) {
       PL::DeleteProducts( $_POST['Delete-checkbox']);
       header("Location: http://ahmedsabry.info");
       }
       if(isset($_POST['Add'])){
       header("Location: http://ahmedsabry.info/addproduct.php");
       }
   ?>
   <form action="<?=$_SERVER['PHP_SELF'];?>" method="POST"> 
      <div class="Products_container">
         <h1>Product List</h1>
         <div class="Buttons">  
            <button type="submit" id="DeleteproductButton" name="Delete"  href='/AllProducts.php'>MASS DELETE</button>  
            <button type="submit" id="AddproductButton" name="Add" text="ADD" onclick=window.location.href='/AddProduct.php'>ADD</button> &nbsp;&nbsp;
         </div>
         <hr/>
         <?php
            echo PL::View_All_Products( );
         ?>  
      </div>
   </form>
   <hr/>
   <p id="footer_text"> Scandiweb Test assignment </p>
   <script src="Scripts/JS.js">  </script>
</body>
   </html>