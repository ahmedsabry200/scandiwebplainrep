
function formload()
{
    var inputfields= document.forms["product_form"].getElementsByTagName("input");
    Array.prototype.forEach.call(inputfields,field=> {
    var validationSpan=   field.nextElementSibling;
    if ((field.getAttribute("type")=="number")) {
            field.addEventListener("input", function (event) {
                if (field.validity.typeMismatch||field.value<0) {
                    field.setCustomValidity("Please, provide positive number");
                    field.setAttribute("class","invalid");
                    validationSpan.innerHTML="Please, provide positive number";
                    event.preventDefault();  
                } else {
                    field.setCustomValidity("");
                    field.removeAttribute("class");
                    validationSpan.innerHTML="";
                }
            });
        }});
    sessionStorage.setItem("AllowValidation", "true");
}
 
function CreateOptionForm(selectObject)
{
    window[selectObject.value+"Option"]();
}
function Option()
{
    var DynamicDiv = document.getElementById("DynamicContent");
    DynamicDiv.innerHTML="";
}
function CraeteInputFieldRow(TextFieldId,TextFieldName,Type,LabelText)
{
    var Row = document.createElement('TR');
    var Labeltd = document.createElement('TD');
    var Label = document.createElement('label');
    Label.setAttribute("for",TextFieldId);
    Label.innerHTML=LabelText;
    Labeltd.appendChild(Label);
    Row.appendChild(Labeltd);
    var Texttd = document.createElement('TD');
    Texttd.setAttribute("class","inputcell");
    var TextInput = document.createElement("input");
    TextInput.setAttribute("id",TextFieldId);
    TextInput.setAttribute("type",Type);
    TextInput.setAttribute("name",TextFieldName);
    var validationSpan = document.createElement("span");
    validationSpan.setAttribute("class","Validation");
    if(Type=="number"){
        TextInput.setAttribute("step","0.01");
        TextInput.setAttribute("min","0.01");
        TextInput.addEventListener("input", function (event) {
            var validationSpan2=   TextInput.nextElementSibling;
            if (TextInput.validity.typeMismatch||TextInput.value<=0) {
                TextInput.setCustomValidity("Please, provide positive number");
                TextInput.setAttribute("class","invalid");
                validationSpan2.innerHTML="Please, provide positive number";
            } else {
                TextInput.setCustomValidity("");
                TextInput.removeAttribute("class");
                validationSpan2.innerHTML="";
            }
        });
    }
    Texttd.appendChild(TextInput);
    Texttd.appendChild(validationSpan);
    Row.appendChild(Texttd);
    return Row;
}

function CraeteTable(ParentElement)
{
    var Dynamictable = document.createElement('TABLE');
    Dynamictable.setAttribute("border","0");
    var tableBody = document.createElement('TBODY');
    Dynamictable.appendChild(tableBody);
    ParentElement.appendChild(Dynamictable);
    return Dynamictable;
}
function CraeteDescriptionRow(DescriptionText)
{
    var Row = document.createElement('TR');
    var td = document.createElement('TD');
    td.setAttribute('colspan','2');
    td.setAttribute('id',"Description");
    td.innerHTML=DescriptionText;
    Row.appendChild(td);
    return Row;
}

function BookOption()
{
    var DynamicDiv = document.getElementById("DynamicContent");
    DynamicDiv.innerHTML = "";
    var BookDiv = document.createElement('div');
    BookDiv.setAttribute("id","Book");
    DynamicDiv.appendChild(BookDiv);
    var tableBody = CraeteTable(BookDiv);
    tableBody.appendChild(CraeteInputFieldRow('weight','Weight','number',"Weight(KG)"));
    tableBody.appendChild(CraeteDescriptionRow("Please provide a weight for Book"));
}

function DVDOption()
{
    var DynamicDiv = document.getElementById("DynamicContent");
    DynamicDiv.innerHTML="";
    var DVDDiv = document.createElement('div');
    DVDDiv.setAttribute("id","DVD");
    DynamicDiv.appendChild(DVDDiv);
    var tableBody =CraeteTable(DVDDiv);
    tableBody.appendChild(CraeteInputFieldRow('size','Size','number',"Size(MB)"));
    tableBody.appendChild(CraeteDescriptionRow("Please provide Size for DVD"));
}

function FurnitureOption()
{
/*******************Create furniture form***************/
    var DynamicDiv = document.getElementById("DynamicContent");
    DynamicDiv.innerHTML = "";
    var FurnitureDiv = document.createElement('div');
    FurnitureDiv.setAttribute("id","Furniture");
    DynamicDiv.appendChild(FurnitureDiv);
    var tableBody =CraeteTable(FurnitureDiv);
    tableBody.appendChild(CraeteInputFieldRow('height','Height','number',"Height (CM)"));
    tableBody.appendChild(CraeteInputFieldRow('width','Width','number',"Width (CM)"));
    tableBody.appendChild(CraeteInputFieldRow('length','Length','number',"Length (CM)"));
    tableBody.appendChild(CraeteDescriptionRow("Please provide furniture dimensions"));
}

function validateIsNotEmptyField(field,FormValidationLabel)
{ 
     var validationSpan=   field.nextElementSibling;
    if (field.value == "") {
        validationSpan.innerHTML = "*";
        FormValidationLabel.innerHTML = "please,submit requied data(*)";
        return false;
    }
    validationSpan.innerHTML = "";
    return true;
}

function validateFields()
{
    if(sessionStorage.getItem("AllowValidation") == "false") {
        return true;
    }
    var FormIsValid=true;
    var inputfields= document.forms["product_form"].getElementsByTagName("input");
    var selectfield= document.getElementById("productType");
    var FormValidationLabel= document.getElementById("FormValidation");
    var result=  Array.prototype.forEach.call(inputfields,inputfield=> {
        if(!validateIsNotEmptyField(inputfield,FormValidationLabel))  
        {
            FormIsValid=false;
        }
        });
    if(!validateIsNotEmptyField(selectfield,FormValidationLabel))
    {
        FormIsValid=false;
    }
    return FormIsValid;
}

function ValidateForNotSelectedSelectField()
{
    var inputfield= document.getElementById("productType");
    var ValidationLabel = document.getElementById("productTypeLabel");
    if (inputfield.value == ""){
        ValidationLabel.innerHTML = "*";
        return true;
    }
    return false;
}

function PreventValidation()
{
    sessionStorage.setItem("AllowValidation", "false");
}
function AllowValidation()
{
    sessionStorage.setItem("AllowValidation", "true");
}




  




