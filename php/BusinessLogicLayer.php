<?php
namespace BusinessLogicLayer;

include ('./php/DataAccessLayer.php');
use DataAccessLayer\DAL;
use DataAccessLayer\MySqlErrors;

class   BLL
{
    //-----------------------products--------------------------
    public static function GetAllProducts()
    {
        return DAL::GetAllProducts();
    }
    public static function GetProductBySku($product)
    {
        return DAL::GetProductBySku($product);
    }
    public static function ProductsCount($product = null)
    {
        return DAL::ProductsCount($product);
    }
    //-----------------------Books-----------------------------
    public static function GetAllBooks()
    {
        return DAL::GetAllBooks();
    } 
    public static function BooksCount($Book = null)
    {
        return DAL::BooksCount($Book);
    }
    //-----------------------furniture------------------------
    public static function GetAllFurnitures()
    {
        return DAL::GetAllFurnitures();
    }
    public static function furnituresCount($Book = null)
    {
        return DAL::furnituresCount($Book);
    }
    //----------------------DVD-------------------------------
    public static function GetAllDVDs()
    {
        return  DAL::GetAllDVDs();
    }
    public static function DVDCount($DVD = null)
    {
        return DAL::DVDsCount($DVD);
    }
}

 //************************product abstract class*******************//
abstract class product 
{
    // Properties
    protected $Sku;
    protected $Name;
    protected $Price;
    protected $Type;
    //setter for Sku 
    public function SetSku($_Sku)
    {
        $this->Sku = $_Sku;
    }
    //getter for Sku 
    public function GetSku()
    {
        return $this->Sku;
    }
    //setter for Name     
    public function SetName($_Name)
    {
        $this->Name = $_Name;
    }
    //getter for Name
    public function GetName()
    {
        return $this->Name;
    }
    //setter for Price 
    public function SetPrice($_Price)
    {
       $this->Price = $_Price;
    }
    //getter for price
    public function Getprice()
    {
      return $this->Price;
    }
    //setter for Type
    protected function SetType($_Type)
    {
       $this->Type = $_Type;
    }
    //getter for Type
    public function GetType()
    {
        return $this->Type;
    }
    //Get SKu,Name,price
    protected function GetBasicData()
    {
        return array('Sku'=>$this->GetSku(), 'Name'=>$this->GetName(), 'Price'=>number_format((float)$this->GetPrice(), 2, '.', '').'$'); 
    }
    public abstract function Fill($Data=null);
    //Display product information
    public abstract function GetData();
    //Add object information to database
    public abstract function AddToDb();
    //Delete this object from database
    public abstract function DeleteFromDB();
}
//*********************book class***************************/

class book extends product
{  
    private $Weight;
    public function __construct( $_Sku, $_Name = null, $_Price = null, $_Weight = null)
    {
        $this->Sku = $_Sku;
        if ($_Name) {
            $this->Name = $_Name;
        }
        if ($_Price) {
            $this->Price = $_Price;
        }
        if ($_Weight) {
            $this->Weight = $_Weight;
        }
        $this->Type = "Book";
    }
    //setter for Weight 
    public function setWeight($_Weight)
    {
        $this->Weight = $_Weight;
    }
    //getter for Weight
    public function GetWeight()
    {
        return $this->Weight;
    }
    //fill the object with Data
    public  function Fill($Data=null)
    {
        if ($Data){
            $this->Sku = $Data['Sku'];
            $this->Name = $Data['Name'];
            $this->Price = $Data['Price'];
            $this->Weight = $Data['Weight'];
            return true;
        } else if ($this->Sku){
            if (!DAL::GetBookBySku($this)) {
                $this->SetSku("");
            }
            else {
               return true;
            }
      
        }
        return false;
    }
    //Get object Data
    public function GetData()
    {
        $data=$this->GetBasicData();
        $data['Weight'] = $this->GetWeight().' KG';
        return $data;
    }
    //Add object information to database
    public  function AddToDb(&$Error_Message=null)
    {
        /*-------------insert without SP---------------------------------------
        if(!DAL::InsertBook($this))
        {
            $Error_Message=MySqlErrors::Get_error();
            return false;
        }
        ----------------------------------------------------------------------*/
        if(!DAL::InsertBookBySP($this))
        {
            $Error_Message = MySqlErrors::Get_error();
            return false;
        }
        return true;
    }
    //Delete this object from database
    public  function DeleteFromDB()
    { 
      return DAL::DeleteProduct($this);
    }
}
 //*********************DVD class**************************** */
class DVD extends product
{
    private $Size;
    public function __construct( $_Sku, $_Name=null, $_Price=null, $_Size=null)
    {
        $this->Sku = $_Sku;
        if ($_Name) {
            $this->Name = $_Name;  
        }
        if ($_Price){
            $this->Price = $_Price;  
        }
        if ($_Size){
            $this->Size = $_Size;  
        }
        $this->Type = "DVD";
    }
    //setter for Size 
    public function setSize($_Size)
    {
       $this->Size = $_Size;
    }
    //getter for Size
    public function GetSize()
    {
      return $this->Size;
    }
    //fill the object with Data
    public  function Fill($Data=null)
    {
        if ($Data){
            $this->Sku = $Data['Sku'];
            $this->Name = $Data['Name'];
            $this->Price = $Data['Price'];
            $this->Size = $Data['Size'];
            return true;
        } else if ($this->Sku) {
            if(!DAL::GetDVDBySku($this) ) 
            {
                $this->SetSku("");
            }else
                return true;
        }
        return false;
    }
    //Get object Data
    public function GetData()
    {
        $data = $this->GetBasicData();
        $data['Size'] = $this->GetSize().' MB';
        return $data;
    }
     //Add object to Database
    public  function AddToDb(&$Error_Message=null)
    {
        /*--------without stored procedure-------------------------
        if(!DAL::InsertDVD($this))
        {
            $Error_Message = MySqlErrors::Get_error();
            return false;
        }
        return true;
        ----------------------------------------------------------*/
        if (!DAL::InsertDVDBySP($this)){
            $Error_Message = MySqlErrors::Get_error();
            return false;
        }
        return true;
    }
    public  function DeleteFromDB()
    {
        return DAL::DeleteProduct($this);
    }
}
    //*******************furniture class****************************** */
class furniture extends product
{
    // Properties
    private $Height;
    private $Width;
    private $Length;
    public function __construct($_Sku, $_Name = null, $_Price = null, $_Height = null, $_Width = null, $_Length = null)
    {
        $this->Sku = $_Sku;
        if ($_Name) {
            $this->Name = $_Name;  
        }       
        if ($_Price){
            $this->Price = $_Price;  
        }
        if ($_Height){
            $this->Height = $_Height;
        }
        if($_Width){
            $this->Width = $_Width;  
        }
        if($_Length){
            $this->Length = $_Length;  
        }
        $this->Type = "Furniture";
    }
    //setter for Height 
    public function SetHeight($_Height)
    {
        $this->Height = $_Height;
    }
    //getter for Height 
    public function GetHeight()
    {
        return $this->Height;
    }
    //setter for Width     
    public function SetWidth($_Width)
    {
        $this->Width = $_Width;
    }
    //getter for Width
    public function GetWidth()
    { 
        return $this->Width;
    }
    //setter for Length 
    public function SetLength($_Length)
    {
        $this->Length = $_Length;
    }
    //getter for Length
    public function GetLength()
    {
        return $this->Length;
    }
    //fill the object with Data
    public  function Fill($Data = null)
    {
        if ($Data) {
            $this->Sku = $Data['Sku'];
            $this->Name = $Data['Name'];
            $this->Price = $Data['Price'];
            $this->Height = $Data['Height'];
            $this->Width = $Data['Width'];
            $this->Length = $Data['Length'];
            return true;
        } else {
            if (! DAL::GetFurnitureBySku($this)) {
                $this->SetSku("");
            }
            else
                return true;
        }
        return false;
    }
    //Display all informations as string
    public function GetData()
    {
        $data = $this->GetBasicData();
        $data['Dimensions'] = $this->GetHeight()."x".$this->GetWidth()."x".$this->GetLength();
        return $data;
    }
    //Add product informations to data base
    public  function AddToDb(&$Error_Message=null)
    {
        /*---------------Add to Data base without stored procedure------------
        if(!DAL::Insertfurniture($this))
        {
            $Error_Message = MySqlErrors::Get_error();
            return false;
        }
        return true;
        --------------------------------------------------------------------*/
        if(!DAL::InsertFurnitureBySP($this))
        {
            $Error_Message = MySqlErrors::Get_error();
            return false;
        }
        return true;
    }
    // Delete this object from Data base
    public  function DeleteFromDB()
    {
        return DAL::DeleteProduct($this);
    }
           
}
 